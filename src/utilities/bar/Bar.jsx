import React from 'react'
import "./Bar.css"
const Bar = (props) => {

  return (
    <article className={props.state?'bar active':"bar"}></article>
  )
}

export default Bar