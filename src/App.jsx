import Home from './components/home/Home'
import { CustomContext } from './javascript/custom-context/CustomContext'
function App() {
  return (
    <main className="App">
      <CustomContext>
        <Home/>
      </CustomContext>
    </main>
  )
}

export default App
