import React from 'react'
import "./Hero.css"
import { useQuery } from '@tanstack/react-query'
import { fetch_upcoming } from '../../javascript/api/apis'
import { AiOutlineInfoCircle } from "react-icons/ai";

const Hero = () => {
    // const query =useQuery(["upcoming"],fetch_upcoming)

    // if(query.isFetching){return <h1>Fetching</h1>}

    // if(query.isError){return <p>Error fetching data</p>}
    // const movies = query.data.results
    // const movie = movies[Math.floor(Math.random()*movies.length)]
    // console.log(movie)
  return (
    <section className='hero' >
      
      {/* <img src={`https://image.tmdb.org/t/p/original/${movie?.backdrop_path}` }alt="" />
      <div className="overlay"></div>

      <h1>{movie?.title}</h1>
      <span>Fecha de lanzamiento{movie?.release_date}</span>
      <p>{movie.overview}</p> */}
      {/* <img src={`https://image.tmdb.org/t/p/original/${movie?.backdrop_path}` }alt="" /> */}
      <img src={`https://www.themoviedb.org/t/p/original/b0PlSFdDwbyK0cf5RxwDpaOJQvQ.jpg` }alt="" />
      <div className="overlay"></div>
      <div className="gradient"></div>

      <div className="hero-content">
      {/* <h1>{movie?.title}</h1> */}
      <h1>The Batman</h1>
      {/* <span>Fecha de lanzamiento {movie?.release_date}</span> */}
      <span>Fecha de lanzamiento 2022-03-01</span>
      {/* <p className='description'>{movie.overview}</p>  */}
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, numquam?</p>
      <div className='info-btn'><AiOutlineInfoCircle className='info-icon' /> Mas información</div>
      </div>
    </section>
  )
}

export default Hero