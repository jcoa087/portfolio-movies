import React from 'react'
import Hero from '../hero/Hero'
import Slider from '../slider/Slider'
import "./Home.css"
const Home = () => {
  return (
    <>
    <Hero/>
    <section className='home'>
      <Slider/>
      <Slider/>
      <Slider/>
      
    </section>
    </>
  )
}

export default Home