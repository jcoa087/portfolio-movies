import React, { useEffect, useRef, useState } from 'react'
import "./Slider.css"
import { AiOutlineInfoCircle } from "react-icons/ai";
import { BiChevronLeft,BiChevronRight } from "react-icons/bi";
import Bar from "./../../utilities/bar/Bar"
import {Debounce}  from "./../../javascript/decorators/Debounce"
import { useModalContext } from '../../javascript/custom-context/CustomContext';
const Slider = () => {
    const slider = useRef(null)
    const track = useRef(null)
    const absolute=useRef(null)
    const [bars,setBars] = useState([])
    const[move,setMove] = useState(0)
    const[slides,setSlides]=useState([])
    const[previous,setPrevious]=useState(0)
    const[modal,setModal] = useModalContext();
    let node
    let copy
    const images = [
        "https://www.themoviedb.org/t/p/w500_and_h282_face/nlCHUWjY9XWbuEUQauCBgnY8ymF.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/3dPhs7hUnQLphDFzdkD407VZDYo.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/3uM41OT0RfBkE6Gb6U89LEskJBr.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/a2n6bKD7qhCPCAEALgsAhWOAQcc.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/kg2FOT2Oe5PSCgs3L4vLel6B7ck.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/xjnxyYqsgRDF9NmuNF19kHHu0Yg.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/tqj7NKj11keFuLzPsBDMUq2dOUO.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/kWYfW2Re0rUDE6IHhy4CRuKWeFr.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/lXhgCODAbBXL5buk9yEmTpOoOgR.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/5vaxg5JTbCAqvcPGT88kQNtKNPR.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/npCPnwDyWfQltGfIZKN6WqeUXGI.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/bVmSXNgH1gpHYTDyF9Q826YwJT5.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/ytdebEE0ndYLSTEctPgh8e0vaBs.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/tmU7GeKVybMWFButWEGl2M4GeiP.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/iNh3BivHyg5sQRPP1KOkzguEX0H.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/zb6fM1CX41D9rF9hdgclu0peUmy.jpg",
        "https://www.themoviedb.org/t/p/w533_and_h300_bestv2/l6hQWH9eDksNJNiXWYRkWqikOdu.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/suaEOtk1N1sgg2MTM7oZd2cfVp3.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/aOHCgF5sEt7aXi8faqEoGKl1vXD.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/8uWbSjUky6NiJnD3Typz6ZN0f9w.jpg", 
        "https://www.themoviedb.org/t/p/w500_and_h282_face/pUPiymnuisHrDaW5wvVlzCzZwD5.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/xRyINp9KfMLVjRiO5nCsoRDdvvF.jpg", 
        "https://www.themoviedb.org/t/p/w500_and_h282_face/xJHokMbljvjADYdit5fK5VQsXEG.jpg", 
        "https://www.themoviedb.org/t/p/w500_and_h282_face/dVr11o9or7AS8AMPfwjSpEU83iU.jpg", 
        "https://www.themoviedb.org/t/p/w500_and_h282_face/ew5FcYiRhTYNJAkxoVPMNlCOdVn.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/dqK9Hag1054tghRQSqLSfrkvQnA.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/Yrpb32j3eMpMVX7ND3TnOkHnbl.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/uS9apevOgs2fuYghf9szOoK1u63.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/ymX3MnaxAO3jJ6EQnuNBRWJYiPC.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/tRS6jvPM9qPrrnx2KRp3ew96Yot.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/5En0fmDagt3Pk8d7P3uTwfeQceg.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/hiKmpZMGZsrkA3cdce8a7Dpos1j.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/aQLygZOIKai6aaiBAeeKpIWO5nc.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/n6bUvigpRFqSwmPp1m2YADdbRBc.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/2lBOQK06tltt8SQaswgb8d657Mv.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/n3UanIvmnBlH531pykuzNs4LbH6.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/AeDS2MKGFy6QcjgWbJBde0Ga6Hd.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/r2rnIplX2gPqt3nkdqqEWVzhTZ8.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/byTvHqQIonluZT76enrwsehb9NW.jpg",
        "https://www.themoviedb.org/t/p/w500_and_h282_face/i51wnN9phojyn14xHjQt6rv1r0x.jpg",
        // "https://www.themoviedb.org/t/p/w500_and_h282_face/aAM3cQmYGjjLQ24m0F0RWfjKQ57.jpg",
        // "https://www.themoviedb.org/t/p/w500_and_h282_face/tMkziWBEHwXs0jdFhOrrW7oCPmR.jpg"
    ]
//El numero de slides se descuadra conforme se va reduciendo el tamaño de la pantalla dado que el numero del mismo dividido entre el numero de slides visibles no es un numero
//cerrado
    useEffect(()=>{
        get_bars()
    },[])
    

    const show_left_button = ()=>{
        setTimeout(() => {
          slider.current.style.setProperty("--btn-event","auto")
          slider.current.style.setProperty("--btn-opacity","1")
          absolute.current.style.setProperty("--slide-event","auto")
          absolute.current.style.setProperty("--slide-opacity","1")
        }, 900); 

      }
    const get_bars=()=>{
        const slides_on_screen = parseInt(getComputedStyle(track.current).getPropertyValue("--slides-on-screen"))
        const slides = track.current.children.length
        const total_bars = Math.round(slides/slides_on_screen)
        let bars_array = []
        for (let index = 0; index < total_bars; index++) {
            bars_array.push(index)  
        }
        setBars(bars_array)
        let slides_array=[]
        for (let index = 0; index < slides_on_screen; index++) {
            slides_array.push(index)
        }
        setSlides(slides_array)
        
    }
    const recalc_bars=Debounce(get_bars)
    useEffect(()=>{
        window.addEventListener("resize",()=>{
            recalc_bars()
        })
    },[])
    const get_current_bars=()=>{
        const slides_on_screen = parseInt(getComputedStyle(track.current).getPropertyValue("--slides-on-screen"))
        const slides = track.current.children.length
        const total_bars = Math.round(slides/slides_on_screen)
        const current_bars=Math.round((move+1)*(total_bars)/(previous))-1
        // console.log("slides")
        // console.log(slides)
        // console.log("move")
        // console.log(move)
        // console.log("total_bars")
        // console.log(total_bars)
        // console.log("previous")
        // console.log(previous)
        // console.log("current_bars")
        // setPrevious(total_bars)


        // console.log(current_bars)
        // console.log(current_bars+1)
        move===0?"":setMove(current_bars)
    }
    const debounce_current_bars = Debounce(get_current_bars,500)
    useEffect(()=>{
        window.addEventListener("resize",debounce_current_bars)
        return ()=>{
            window.removeEventListener("resize",debounce_current_bars)
        }
    },[move])
    //Solo revisa el de la izquiera, el boton derecho funciona bien
    const left = ()=>{
        const slides_on_screen = parseInt(getComputedStyle(track.current).getPropertyValue("--slides-on-screen"))
        const slides_total = track.current.children.length
        const total_bars = Math.floor(slides_total/slides_on_screen)-1
        const last=bars.length-1
        let aux_array = []
        track.current.style.transform="translateX(100%)"
        track.current.style.transition="1s"
        absolute.current.style.transform="translateX(100%)"
        absolute.current.style.transition="1s"
        // const track_length = track.current.children.length-1
        // const node_index = track_length-(slides.length+1)
        setTimeout(() => {
            track.current.style.transform="translateX(00%)"
            track.current.style.transition="none"
            absolute.current.style.transform="translateX(00%)"
            absolute.current.style.transition="none"

            // console.log(track.current.children.length)
            while (absolute.current.firstChild) {
                absolute.current.firstChild.remove()
            }
            slides.map((bar)=>{
                track.current.insertAdjacentElement('afterbegin',track.current.lastChild)
                node = track.current.children[39-slides.length+1]
                copy = node.cloneNode(true)
                aux_array.push(copy)
            })

            aux_array.map((nod)=>{
                absolute.current.append(nod)
            })
            node = track.current.children[39-slides.length]
            copy = node.cloneNode(true)
            absolute.current.append(copy)
            move <= 0 ? setMove(last) : setMove(move=>move-1)
            
            setPrevious(total_bars) 
        }, 1000);
    
    }
    const debounce_left=Debounce(left,500)
    const right=()=>{
        const slides_on_screen = parseInt(getComputedStyle(track.current).getPropertyValue("--slides-on-screen"))
        const slides_total = track.current.children.length
        const total_bars = Math.floor(slides_total/slides_on_screen)-1
        const last=bars.length-1
        track.current.style.transform="translateX(-100%)"
        track.current.style.transition="1s"
        const track_length = track.current.children.length
        const node_index = track_length-(slides.length+1)
        setTimeout(() => {
            track.current.style.transform="translateX(0%)"
            track.current.style.transition="none"
            let aux_array = []
          
            while (absolute.current.firstChild) {
                absolute.current.firstChild.remove()
            }
            slides.map((bar)=>{
                track.current.insertAdjacentElement('beforeend',track.current.firstChild)
                node = track.current.children[39]
                copy = node.cloneNode(true)
                aux_array.push(copy)
            })
          
            const rev = aux_array.reverse()
            rev.map((nod)=>{
                absolute.current.append(nod)
            })
            node = track.current.children[node_index]
            copy = node.cloneNode(true)
            absolute.current.append(copy)

            move === last? setMove(0) :setMove(move=>move+1)
            setPrevious(total_bars+1) 
        }, 1000);
        show_left_button()
    }
    const debounce_right=Debounce(right,500)
    const openModal = ()=>{
        setModal(!modal)
    }
    return (
    <section className='slider' ref={slider}>
        {/* <div className="bars">
            {
                bars.map((bar,idx)=>
                <Bar state={move === bar?true:false} key={idx} />
                )
            }
        </div> */}
        <div className="left-btn"  onClick={()=>debounce_left()}>
            <BiChevronLeft className='left-icon'/>
        </div> 
        <div className="right-btn" onClick={()=>debounce_right()}><BiChevronRight className='right-icon'/></div>       
        <div className="track absolute" ref={absolute}>
  
        </div> 
        <div className="track" ref={track}  >
            {
                images.map((image,idx)=>
                <div className="slide" key={idx}>
                    <div className="slide-overlay">
                        <div className='info-btn' onClick={()=>openModal()}><AiOutlineInfoCircle className='info-icon' /> Mas información</div>
                    </div>
                    <img src={image} alt="" />
                </div>
                )
            }
        </div>
    </section>
    )
}

export default Slider
