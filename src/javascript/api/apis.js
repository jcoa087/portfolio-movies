import KEY from "./key";
import axios from "axios";

export const fetch = async()=>{
    try{
        const res = await axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=${KEY}&language=es-MX`)
        return res.data
    }catch(error){
        console.error(error)
    }
}

export const fetch_upcoming = async()=>{
    try{
        const res = await axios.get(`https://api.themoviedb.org/3/movie/upcoming?api_key=${KEY}&language=es-MX&size=42`)
        return res.data
    }catch(error){
        console.error(error)
    }
}