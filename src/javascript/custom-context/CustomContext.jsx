import React,{createContext,useContext,useState} from "react";

const context = createContext();


export const useModalContext=()=>{
    return useContext(context);
}

export const CustomContext = ({children})=>{
    const [modal,setModal]=useState(false)

    return(
        <context.Provider value={[modal,setModal]}>
            {children}
        </context.Provider>
    )
}